/*  
 * ------------------------------------------------------------------------
 * "PH2LB LICENSE" (Revision 1) : (based on "THE BEER-WARE LICENSE" Rev 42) 
 * <lex@ph2lb.nl> wrote this file. As long as you retain this notice
 * you can do modify it as you please. It's Free for non commercial usage 
 * and education and if we meet some day, and you think this stuff is 
 * worth it, you can buy me a beer in return
 * Lex Bolkesteijn 
 * ------------------------------------------------------------------------ 
 * Filename : TTN_Enschede_PMS_Node.ino  
 * Version  : 1.0 (BETA)
 * ------------------------------------------------------------------------
 * Description : A low power Plantower PMS5003 based datalogger for.
 *  the ThingsNetwork with deepsleep support and variable interval
 * ------------------------------------------------------------------------
 * Revision : 
 *  - 2018-jul-29 1.0 first "beta"
 * ------------------------------------------------------------------------
 * Hardware used : 
 *  - Arduino Pro-Mini 3.3V 
 *        Board: ATmega328
 *        Clock: 8MHz external
 *        Compiler: LTO: Disabled (default)
 *        Variant: "328P / 328PA"
 *        BOD: 1.8v
 *        
 *        Programmer USBasp
 *        
 *  - RFM95W
 *  - Plantower PMS5003
 * ------------------------------------------------------------------------
 * Software used : 
 *  - LMIC https://github.com/matthijskooijman/arduino-lmic 
 *  - LowPower library https://github.com/rocketscream/Low-Power
 *  - MiniCore loader  https://forum.arduino.cc/index.php?topic=412070 https://github.com/MCUdude/MiniCore 
 *    To use a brownout detection of 1.8V.
 *  - special adcvcc library from Charles (see : https://www.thethingsnetwork.org/forum/t/full-arduino-mini-lorawan-and-1-3ua-sleep-mode/8059/32?u=lex_ph2lb )
 *  - PMS5003 https://learn.adafruit.com/pm25-air-quality-sensor/arduino-code
 *            http://aqicn.org/sensor/pms5003-7003/
 *            http://www.aqmd.gov/docs/default-source/aq-spec/resources-page/plantower-pms5003-manual_v2-3.pdf
 *            
 *            https://github.com/jbanaszczyk/pms5003 (alternatief?)
 *  
 * For licenses of the used libraries, check the links above.
 * ------------------------------------------------------------------------ 
 * Aditional note : 
 * 
 * I use a HTTP integration on the TTN with the decoder below.
 * Or you can use Cayenne 
 * ------------------------------------------------------------------------ 
 * TODO LIST : 
 *  - add more sourcode comment
 *  
 *  
 **************************************************************************************** 
 */

 
/*
*****************************************************************************************
* INCLUDE FILES
*****************************************************************************************
*/
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <LowPower.h>
#include <Wire.h> 
#include  "adcvcc.h"  

#define USE_PMS 1

#ifdef USE_PMS
#include "PMS.h"
#endif

#include <SoftwareSerial.h>
SoftwareSerial pmsSerial(4, 5); // RX, TX (blue, white)

//#define USE_CAYENNE 1

#ifdef USE_CAYENNE
#include "CayenneLPP.h"
CayenneLPP lpp(51);                    // create a buffer of 51 bytes to store the payload
#endif


/*
 * Defines
 */ 
#define VERSION "1.1" 

#define debugSerial Serial 
#define SHOW_DEBUGINFO  
#define debugPrintLn(...) { if (debugSerial) debugSerial.println(__VA_ARGS__); }
#define debugPrint(...) { if (debugSerial) debugSerial.print(__VA_ARGS__); } 
#define debugFlush() { if (debugSerial) debugSerial.flush(); } 

// When enable BROWNOUTDISABLED be sure that you have BOD fused.
// #define BROWNOUTDISABLED

 #define NORMALINTERVAL  900   // 15 minutes (normal)  
//#define NORMALINTERVAL  300   // 5 minutes (normal)  
// #define NORMALINTERVAL  60   // 1 minute (debug)  

// Restrict to channel0   if uncommented; otherwise all channels  (allways use SF7)
// #define CHANNEL0
 
//// Choice on of the LMIC pinnings below.

//// Pin mapping for TTN Enschede board
//#define LMIC_NSS    10
//#define LMIC_RXTX   6
//#define LMIC_RST    LMIC_UNUSED_PIN
//#define LMIC_DIO0   4
//#define LMIC_DIO1   5
//#define LMIC_DIO2   7

// Pin mapping CH2I (check out : https://www.thethingsnetwork.org/forum/t/full-arduino-mini-lorawan-and-1-3ua-sleep-mode/8059 ) 
#define LMIC_NSS    10
#define LMIC_RXTX   LMIC_UNUSED_PIN
#define LMIC_RST    LMIC_UNUSED_PIN
#define LMIC_DIO0   2
#define LMIC_DIO1   7
#define LMIC_DIO2   8

// Pin mapping PMS5003 sensor
#define PMS_TX      4    // check out: SoftwareSerial pmsSerial(4, 5); above
#define PMS_RX      5


const lmic_pinmap lmic_pins = {
    .nss = LMIC_NSS,
    .rxtx = LMIC_RXTX,   
    .rst = LMIC_RST,
    .dio = {LMIC_DIO0, LMIC_DIO1, LMIC_DIO2},  
}; 

#ifdef USE_PMS
PMS pms(pmsSerial);
PMS::DATA data;
#else
struct pms5003data {
  uint16_t framelen;
  uint16_t pm10_standard, pm25_standard, pm100_standard;
  uint16_t pm10_env, pm25_env, pm100_env;
  uint16_t particles_03um, particles_05um, particles_10um, particles_25um, particles_50um, particles_100um;
  uint16_t unused;
  uint16_t checksum;
};

struct pms5003data PMSdata;
#endif


//// UPDATE WITH YOURE TTN KEYS AND ADDR
//static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // LoRaWAN NwkSKey, network session key 
//static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00A6, 0x00 }; // LoRaWAN AppSKey, application session key 
//static const u4_t DEVADDR = 0x00000000 ; // LoRaWAN end-device address (DevAddr)
 
// Keys for Application cb_fijnstof Device pms5003
static const PROGMEM u1_t NWKSKEY[16] = { 0x27, 0xDE, 0x0F, 0x6C, 0x34, 0x3F, 0xDC, 0x0D, 0xC6, 0x87, 0x4D, 0x1A, 0x68, 0xB7, 0x29, 0x98 }; // LoRaWAN NwkSKey, network session key 
static const u1_t PROGMEM APPSKEY[16] = { 0x3A, 0xD8, 0x29, 0xA6, 0xDE, 0xDC, 0x28, 0x3C, 0x7D, 0x98, 0x68, 0xFB, 0x27, 0xC7, 0x30, 0x76 }; // LoRaWAN AppSKey, application session key 
static const u4_t DEVADDR = 0x26011F5E ;   // LoRaWAN end-device address (DevAddr)
 

// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }
 
static osjob_t sendjob; 

// global enviromental parameters
static float temp = 15.0;
static float pressure = 1013.25;
static float humidity = 45.0;    

int interval = NORMALINTERVAL;  

byte LMIC_transmitted = 0;
int LMIC_event_Timeout = 0;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
// const unsigned TX_INTERVAL = 60; 

/* ======================================================================
Function: ADC_vect
Purpose : IRQ Handler for ADC 
Input   : - 
Output  : - 
Comments: used for measuring 8 samples low power mode, ADC is then in 
          free running mode for 8 samples
====================================================================== */
ISR(ADC_vect)  
{
  // Increment ADC counter
  _adc_irq_cnt++;
}


void updateEnvParameters()
{
//    delay(1000); 
//    temp = bme.temp(true);
//    pressure = bme.pres(1);    // 1 = hPa (milliBar)  
//    humidity =  bme.hum(); 
#ifdef USE_PMS
  debugPrintLn("Waking up, wait 30 seconds for stable readings...");
  pms.wakeUp();
  delay(30000);

  // Clear buffer (removes potentially old data) before read. Some data could have been also sent before switching to passive mode.
  while (pmsSerial.available()) { pmsSerial.read(); }

//  debugPrintLn("Send read request...");
  pms.requestRead();

  debugPrintLn("Wait max. 1 second for read...");
  delay(1000);
  if (pms.readUntil(data))
  {
//    debugPrint("PartMat 1.0 (ug/m3): ");
//    debugPrintLn(data.PM_AE_UG_1_0);

//    debugPrint("PartMat 2.5 (ug/m3): ");
//    debugPrintLn(data.PM_AE_UG_2_5);

//    debugPrint("PartMat 10.0 (ug/m3): ");
//    debugPrintLn(data.PM_AE_UG_10_0);
  }
  else
  {
    debugPrintLn("No data.");
  }

  debugPrintLn("Sleep 60s.");
  pms.sleep();
#else
  if (readPMSdata(&pmsSerial))
  {
//    debugPrint("PM 1.0 (ug/m3): ");
//    debugPrintLn(PMSdata.pm10_standard);

//    debugPrint("PM 2.5 (ug/m3): ");
//    debugPrintLn(PMSdata.pm25_standard);

//    debugPrint("PM 10.0 (ug/m3): ");
//    debugPrintLn(PMSdata.pm100_standard);

//    debugPrintLn();
  }
#endif
} 

void onEvent (ev_t ev) 
{
    debugPrint(os_getTime());
    debugPrint(": ");
    debugPrintLn(ev);
    switch(ev) 
    {
        case EV_SCAN_TIMEOUT:
            //debugPrintLn(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            //debugPrintLn(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            //debugPrintLn(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            //debugPrintLn(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            //debugPrintLn(F("EV_JOINING"));
            break;
        case EV_JOINED:
            //debugPrintLn(F("EV_JOINED"));
            break;
        case EV_RFU1:
            //debugPrintLn(F("EV_RFU1"));
            break;
        case EV_JOIN_FAILED:
            //debugPrintLn(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            //debugPrintLn(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            debugPrintLn(F("EV_TXC"));
            if (LMIC.txrxFlags & TXRX_ACK)
              debugPrintLn(F("R ACK")); // Received ack
            if (LMIC.dataLen) 
            {
              debugPrintLn(F("R "));
              debugPrintLn(LMIC.dataLen);
              debugPrintLn(F(" bytes")); // of payload
            }            
            // Schedule next transmission
            // os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            LMIC_transmitted = 1; 
            break;
        case EV_LOST_TSYNC:
            //debugPrintLn(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            //debugPrintLn(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            //debugPrintLn(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            //debugPrintLn(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            //debugPrintLn(F("EV_LINK_ALIVE"));
            break;
         default:
            //debugPrintLn(F("Unknown event"));
            break;
    }
}

void do_send(osjob_t* j)
{
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) 
    {
        debugPrintLn(F("OP_TXRXPEND")); //P_TXRXPEND, not sending
    } 
    else 
    {
        // Prepare upstream data transmission at the next possible time.
 
        // Here the sensor information should be retrieved 
        //  Pressure: 300...1100 hPa
        //  Temperature: -40…85°C  
        updateEnvParameters();
 
        int batt = (int)(readVcc() / 100);  // readVCC returns  mVolt need just 100mVolt steps
        byte batvalue = (byte)batt; // no problem putting it into a int. 
       
#ifdef SHOW_DEBUGINFO
        debugPrint(F("T="));
        debugPrintLn();
        
       delay(250);
        debugPrint("PM 1.0 (ug/m3): ");
//        debugPrintLn(PMSdata.pm10_standard);
        debugPrintLn(data.PM_AE_UG_1_0);

       delay(250);
        debugPrint("PM 2.5 (ug/m3): ");
//        debugPrintLn(PMSdata.pm25_standard);
        debugPrintLn(data.PM_AE_UG_2_5);

       delay(250);
        debugPrint("PM 10.0 (ug/m3): ");
//        debugPrintLn(PMSdata.pm100_standard);
        debugPrintLn(data.PM_AE_UG_10_0);
    
       delay(250);
        debugPrint(F("B="));
        debugPrintLn(batt);
        debugPrint(F("BV="));
        debugPrintLn(batvalue);  
#endif

#ifdef USE_CAYENNE
        lpp.reset();                              // clear the buffer
        lpp.addTemperature(1, temp);              // on channel 1, add temperature, value 22.5°C
        lpp.addBarometricPressure(2, pressure);   // channel 2, pressure
        lpp.addRelativeHumidity(3, humidity);     // channel 3, pressure
        float vcc = (float)readVcc()/1000.0;
        lpp.addAnalogInput(4, vcc);               // channel 4, battery voltage
        lpp.addLuminosity(5, data.PM_AE_UG_1_0);  // channel 5, PM 1.0 ug/m3
        lpp.addLuminosity(6, data.PM_AE_UG_2_5);  // channel 6, PM 2.5 ug/m3
        lpp.addLuminosity(7, data.PM_AE_UG_10_0); // channel 7, PM 10 ug/m3

        LMIC_setTxData2(1, lpp.getBuffer(), lpp.getSize(), 0);
#else
        int t = (int)((temp + 40.0) * 10.0); 
        // t = t + 40; => t [-40..+85] => [0..125] => t = t * 10; => t [0..125] => [0..1250]
        int p = (int)(pressure);  // p [300..1100]
        int h = (int)(humidity);

        unsigned char mydata[13];
        mydata[0] =  batvalue;      
        mydata[1] = h & 0xFF; 
        mydata[2] = t >> 8;
        mydata[3] = t & 0xFF;
        mydata[4] = p >> 8;
        mydata[5] = p & 0xFF;  
        //
        mydata[6]  = data.PM_AE_UG_1_0 >> 8; 
        mydata[7]  = data.PM_AE_UG_1_0 & 0xFF; 
        mydata[8]  = data.PM_AE_UG_2_5 >> 8;
        mydata[9]  = data.PM_AE_UG_2_5 & 0xFF;
        mydata[10] = data.PM_AE_UG_10_0 >> 8;
        mydata[11] = data.PM_AE_UG_10_0 & 0xFF;  
        mydata[12] = 0x00;  

        LMIC_setTxData2(1, mydata, sizeof(mydata), 0);
#endif
        debugPrintLn(F("PQ")); //Packet queued
    }
    // Next TX is scheduled after TX_COMPLETE event.
}
 

bool bootFromBrownOut = false;

// shows the bootstatus on serial output and set bootFromBrownout flag.
void showBootStatus(uint8_t _mcusr)
{
  debugPrint(F("mcusr = "));
  debugPrint(_mcusr, HEX);
  debugPrint(F(" > "));
  if (_mcusr & (1<<WDRF))
  {
    debugPrint(F(" WDR"));
    _mcusr &= ~(1<<WDRF);
  }
  if (_mcusr & (1<<BORF))
  {
    debugPrint(F(" BOR"));
    _mcusr &= ~(1<<BORF);
    bootFromBrownOut = true;         // soms geeft dit problemen (brownout wordt niet goed gezien) zet hem dan op false.
  }
  if (_mcusr & (1<<EXTRF))
  {
    debugPrint(F(" EXTF"));
    _mcusr &= ~(1<<EXTRF);
  }
  if (_mcusr & (1<<PORF))
  {
    debugPrint(F(" POR"));
    _mcusr &= ~(1<<PORF);
  }
  if (_mcusr != 0x00)
  {
    // It should never enter here
    debugPrint(F(" ??"));
  }
  debugPrintLn("");
}

void setup() 
{
    uint8_t mcusr = MCUSR;
    MCUSR = 0;
#ifndef USE_PMS
    memset((void *)&PMSdata, 0, 30);
#endif
  
    Serial.begin(9600);   // GPIO1, GPIO3 (TX/RX pin on ESP-12E Development Board)

    // sensor baud rate is 9600
    pmsSerial.begin(9600);
    pmsSerial.setTimeout(1500);
#ifdef USE_PMS
    pms.passiveMode();    // Switch to passive mode
#endif
    
    debugPrint(F("B "));  
    debugPrintLn(F(VERSION));  
    showBootStatus(mcusr);

#ifdef BROWNOUTDISABLED
    // warning when using code below, disable brownout detection.
    // For Arduino Pro Mini 8Mhz 3.3V
    // MiniCore : ATMega328
    // Clock : 8Mhz external
    // Compiler LTO : Enabled
    // Variant : 328P/328PA
    // BOD : Disabled
    int batt =  readVcc(); // do initial read, readVCC returns  mVolt  
    debugPrint(F("B="));
    debugPrintLn(batt); 
    while (batt < 1800)
    { 
        debugPrintLn(F("low power !"));
        LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_ON);  
        batt = readVcc(); // readVCC returns  mVolt    
    }  
    // until here   
#endif
  
    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();

    // Set static session parameters. Instead of dynamically establishing a session
    // by joining the network, precomputed session parameters are be provided.
    #ifdef PROGMEM
    // On AVR, these values are stored in flash and only copied to RAM
    // once. Copy them to a temporary buffer here, LMIC_setSession will
    // copy them into a buffer of its own again.
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
    // If not running an AVR with PROGMEM, just use the arrays directly
    LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif

    #if defined(CFG_eu868)
    // Set up the channels used by the Things Network, which corresponds
    // to the defaults of most gateways. Without this, only three base
    // channels from the LoRaWAN specification are used, which certainly
    // works, so it is good for debugging, but can overload those
    // frequencies, so be sure to configure the full frequency range of
    // your network here (unless your network autoconfigures them).
    // Setting up channels should happen after LMIC_setSession, as that
    // configures the minimal channel set.
    // NA-US channels 0-71 are configured automatically
    LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
    LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band


    // For single channel gateways: Restrict to channel 0 when defined above
#ifdef CHANNEL0
    LMIC_disableChannel(1);
    LMIC_disableChannel(2);
    LMIC_disableChannel(3);
    LMIC_disableChannel(4);
    LMIC_disableChannel(5);
    LMIC_disableChannel(6);
    LMIC_disableChannel(7);
    LMIC_disableChannel(8);
#endif

    // TTN defines an additional channel at 869.525Mhz using SF9 for class B
    // devices' ping slots. LMIC does not have an easy way to define set this
    // frequency and support for class B is spotty and untested, so this
    // frequency is not configured here.
    #elif defined(CFG_us915)
    // NA-US channels 0-71 are configured automatically
    // but only one group of 8 should (a subband) should be active
    // TTN recommends the second sub band, 1 in a zero based count.
    // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
    LMIC_selectSubBand(1);
    #endif

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    LMIC_setDrTxpow(DR_SF7,14); 
    debugPrintLn(F("S")); // Setup complete!"
    debugFlush();   
}

void golowpower()
{
    LMIC_shutdown();
}

void loop() 
{ 
    if (!bootFromBrownOut)
    {
        // Start job
        do_send(&sendjob);
        // Wait for response of the queued message (check if message is send correctly)
        os_runloop_once();
        // Continue until message is transmitted correctly 
        debugPrintLn(F("W")); // aiting for transmittion
        LMIC_event_Timeout = 60*100;  // 60 * 100 times 10mSec = 60 seconds
        while(LMIC_transmitted != 1) 
        {
            os_runloop_once();
            // Add timeout counter when nothing happens: 
            delay(10);
            if (LMIC_event_Timeout-- == 0) 
            {
                // Timeout when there's no "EV_TXCOMPLETE" event after 60 seconds
                debugPrintLn(F("ETO, msg not tx"));
                break;
            } 
        }
    }
    else
    {
       debugPrintLn("X");
    }
    bootFromBrownOut = false;

    LMIC_transmitted = 0;
    LMIC_event_Timeout = 0;
    debugPrintLn(F("G"));
    debugFlush();
    
    for (int i = 0; i < interval; i++)
    {  
          i +=8 ; // no normal 1 second run but 8 second loops m.      
          // Enter power down state for 8 s with ADC and BOD module disabled
          LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);   
    }  
    debugPrintLn("-");
}

#ifdef USE_PMS
#else
boolean readPMSdata(Stream *s) {
  if (! s->available()) {
    return false;
  }
  
  // Read a byte at a time until we get to the special '0x42' start-byte
  if (s->peek() != 0x42) {
    s->read();
    return false;
  }

  // Now read all 32 bytes
  if (s->available() < 32) {
    return false;
  }
    
  uint8_t buffer[32];    
  uint16_t sum = 0;
  s->readBytes(buffer, 32);

  // get checksum ready
  for (uint8_t i=0; i<30; i++) {
    sum += buffer[i];
  }

//  /* debugging
  for (uint8_t i=2; i<32; i++) {
    Serial.print("0x"); Serial.print(buffer[i], HEX); Serial.print(", ");
  }
  Serial.println();
//  */
  
  // The data comes in endian'd, this solves it so it works on all platforms
  uint16_t buffer_u16[15];
  for (uint8_t i=0; i<15; i++) {
    buffer_u16[i] = buffer[2 + i*2 + 1];
    buffer_u16[i] += (buffer[2 + i*2] << 8);
  }

  // put it into a nice struct :)
  memcpy((void *)&PMSdata, (void *)buffer_u16, 30);

  if (sum != PMSdata.checksum) {
    debugPrintLn("Checksum failure");
    return false;
  }
  // success!
  return true;
}
#endif
